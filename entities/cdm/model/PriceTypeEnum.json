{
  "classifierPath" : "meta::pure::metamodel::type::Enumeration",
  "content" : {
    "_type" : "Enumeration",
    "name" : "PriceTypeEnum",
    "package" : "cdm::model",
    "taggedValues" : [ {
      "tag" : {
        "profile" : "meta::pure::profiles::doc",
        "value" : "doc"
      },
      "value" : "Provides enumerated values for types of prices in the Price data type in order to explain how to interpret the amount and use it in calculations."
    } ],
    "values" : [ {
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "Denotes interest accrued between payments, represented as a decimal, for example the accrued interest associated with a bond trade."
      } ],
      "value" : "AccruedInterest"
    }, {
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "Denotes the maximum allowable level of a floating rate for the calculation period, which is used for a cap rate contractual product or in the context of a floating leg.  The CapRate is assumed to be exclusive of any spread, and is defined as a per annum rate expressed as a decimal, for example, the value of 0.05 is the equivalent of 5.0%."
      } ],
      "value" : "CapRate"
    }, {
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "Denotes a price expressed as a cash amount for an upfront fee or other purposes. For example, {amount, unitOfAmount, PerUnitOfAmount} = [12,500, USD, null] = USD 12,500."
      } ],
      "value" : "CashPrice"
    }, {
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "Denotes a bond price without accrued interest."
      } ],
      "value" : "CleanPrice"
    }, {
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "Denotes the net price excluding accrued interest. The Dirty Price for bonds is put in the netPrice element, which includes accrued interest. Thus netPrice - cleanNetPrice = accruedInterest. The currency and price expression for this field are the same as those for the (dirty) netPrice."
      } ],
      "value" : "CleanNetPrice"
    }, {
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "Denotes the amount of commission on the trade."
      } ],
      "value" : "Commission"
    }, {
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "Denotes a bond price with accrued interest."
      } ],
      "value" : "DirtyPrice"
    }, {
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "Denotes a discount factor expressed as a decimal, e.g. 0.95."
      } ],
      "value" : "Discount"
    }, {
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "Denotes an all-in-rate (Spot plus forward if applicable) to convert one currency or other measure of value to another.  Foreign Excahnge rates are represented in decimals, e.g. {amount, unitOfAmount, PerUnitOfAmount} = [1.23, USD, GBP] = USD 1.23 for every 1 GBP."
      } ],
      "value" : "ExchangeRate"
    }, {
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "Denotes the minimum allowable level of a floating rate for the calculation period. Can be used for a floor rate contractual product or in the context of a floating leg. The cap rate is assumed to be exclusive of any spread and is a per annum rate, expressed as a decimal.  For example, a floorRate value of 0.05 is the equivalent of 5.0%."
      } ],
      "value" : "FloorRate"
    }, {
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "Denotes the points to be added to a spot price to represent a forward price, expressed as a decimal. For example for a foreign exchange forward trade."
      } ],
      "value" : "ForwardPoints"
    }, {
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "Denotes a negotiated price for a security or listed product, including as applicable any commissions, discounts, accrued interest, and rebates."
      } ],
      "value" : "GrossPrice"
    }, {
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "Denotes an interest rate to be applied to quantity of notional amount, interest rates are usually quoted as annualised rates and represented as decimal, e.g. {amount, unitOfAmount, PerUnitOfAmount} = [0.015, USD, USD] = 1.5% of the USD notional quantity/amount (or 1.5 cents for every USD of notional amount)."
      } ],
      "value" : "InterestRate"
    }, {
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "Denotes a value to be multiplied by the observed index value to scale it before adding a spread."
      } ],
      "value" : "MultiplierOfIndexValue"
    }, {
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "Denotes a negotiated price for a security or listed product - excluding as applicable any commissions, discounts, accrued interest, and rebates."
      } ],
      "value" : "NetPrice"
    }, {
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "Denotes a price expressed in percentage of face value with fractions which is used for quoting bonds, e.g. 101 3/8 indicates that the buyer will pay 101.375 of the face value."
      } ],
      "value" : "ParValueFraction"
    }, {
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "Denotes the amount payable by the buyer to the seller for an option.  The premium is paid on the specified premium payment date or on each premium payment date if specified."
      } ],
      "value" : "Premium"
    }, {
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "Denotes a price expressed as a rate to be applied to quantity/notional amount and represented as decimal, e.g. {amount, unitOfAmount, PerUnitOfAmount} = [0.08, EUR, EUR] = 8%  of the EUR notional quantity/amount or 8 cents for every EUR of notional amount."
      } ],
      "value" : "RatePrice"
    }, {
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "Denotes a price expressed as a decimal for the purposes of calculating the Physical Settlement Amount or Cash Settlement Amount on a Credit Derivative transaction."
      } ],
      "value" : "ReferencePrice"
    }, {
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "Denotes price/rate for a near-immediate delivery according to the conventions of a market, for example, in the commodities market."
      } ],
      "value" : "Spot"
    }, {
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "Denotes a difference in interest rates or prices expressed as a decimal, for example, in the case of a spread between two interest rates, the value of 0.05 is the equivalent of 500 basis points or 5.0%."
      } ],
      "value" : "Spread"
    } ]
  }
}